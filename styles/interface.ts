export interface IBlock {
  readonly "block": string;
}
export interface IComponents {
  readonly "image": string;
}
export interface ISection {
  readonly "section": string;
}
/* tslint:disable:max-line-length */
export type IValueBlocks = "block" | "image" | "section";
export type IValueElements = "";
export type IValueMods = "";
/* tslint:enable:max-line-length */
